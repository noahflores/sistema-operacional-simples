org 0x7c00
bits 16


jmp short main
nop

;bpb paramentos de bios
fat_eom db 'MSWIN4.1'; credecias do fabricante
block_disk dw 512
sector_p_cluters db 1
sector_reserve dw 1
fat_table db 2
number_end dw 0xe0; maximo de entradas
space_disk dw 2880
label_disk db 0xf0
sector_fat dw 9
sector_p_track dw 18
head_face dw 2
sector_hide dd 0
big_sector dd 0

;bpb estendido
n_driver db 0
reserv_winnt db 0; reservado para windows nt
sig_defa db 0x29; assinatura padrão pode se 0x29 ou 0x28
number_serie dd 0x12345678
label_system db 'system simp'
system_part db 'fat12   '



main:

;limpando os secremntos usando o ax
xor ax, ax
mov ds, ax
mov es, ax
mov ss, ax
mov sp, 0x7c00

.prt:

;limpando a tela
mov al, 0x03; modo de video
int 0x10

mov si, msg
mov ah, 0x0e
mov cx, msg_len


.read_char:
lodsb
int 0x10
loop .read_char

;a parti daqui começa a carrega o kernel

;calcular o inicio segmento 3 
mov ax, [sector_fat];multiplicação setores de fat por tabelas
mov bl, [fat_table]
xor bh, bh
mul bx
add ax ,[sector_reserve]; ax recebe lba do diretorio raiz
push ax ; salvar a lba

;começa o calculo do tamanho da pasta usando 32 bytes de entrada  * maximo de entradas / block
mov ax, [number_end]
shl ax , 5 ;desloca 5 a esquerda por se multiplo de 5 é a mesma coisa 5 sobe n
xor dx, dx
div word [block_disk] ; dividi registra o quociente no ax
test dx, dx ; se houver resto tem que arredonda para cima
jz read_dir_root; caso não tem resto
inc ax

read_dir_root:
mov cl , al ;passa o tamanho do diretorio para cl
pop ax; salva o tamanho do diretorio na pilha
mov dl, [n_driver]; numero do driver do disco
mov bx, buffer; passa o endereço do buffer
call disk_read

;começa a parte de localiza o nome do arquivo kernel
xor bx, bx
mov di , buffer; recebe o buffer para comparação

find_kernel:

mov si, name_kernel; passa o nome do kernel no si
mov cx, 11 ; tamanho dos bytes em fat
push di; salva o endereço do buffer na pilha
repe cmpsb; Compara sequencialmente cada byte em SI com os bytes com
          ; os bytes no endereço do buffer (DI) até o valor em CX.
pop di; restaura o di
je kernel_found; o kernel foi encontrado

add di, 32 ;caso não encontra emtra em loop até estoura o máximo de entradas
inc bx
cmp bx , [number_end] 
jl find_kernel


kernel_not_found:

mov si, kernel_not
mov ah, 0x0e
mov cx, msg5_len


.read_char:
lodsb
int 0x10
loop .read_char
jmp fatal_error

kernel_found:;check

mov ax, [di + 26] ;coloca a entrada com mais 26 bytes
mov [kernel_cluters], ax ;move o ax para o cluters primario do kernel

;transferir a tabela do fat para o buffer

mov ax , [sector_reserve]; passa a ax o lba do setor reserva
mov bx, buffer
mov cl , [sector_fat]
mov dl, [n_driver]
call disk_read

;redefinindo o segmento de leitura
; aviso se de erro remova o [] do rotulo quando estive rotulando equ
mov bx, kernel_segment;passando o segmento 
mov es, bx; usando o estendio para escrita em vez do buffer padrão
mov bx, kernel_offset; passando o offset


load_kernel_loop:;check

mov ax, [kernel_cluters]; passando o primeiro cluter do arquivo
add ax, 31; passando a primeira entrada do diiretorio

mov cl, 1; ler um setor de dados por vez
mov dl, [n_driver]; número de drives
call disk_read

add bx, [block_disk]; incrementa no estendido os blocks do setor

mov ax, [kernel_cluters]; restaura o cluters
mov cx, 3;o cluters seguinte estará em 1,5 bytes do fat 12
mul cx; a multiplicação por 3 e dividindo por 2 pemiti um arredondamento
mov cx, 2
div cx

mov si, buffer; passa o buffer com a tabela fat em si
add si , ax ; soma o cluters inicial no si
mov ax, [ds:si] ;passa o conteudo de 2 bytes no ax
test dx, dx ; verica se tever resto
jz is_oven; se for 0 pela lógica de 3 por 2 o indice é par


is_odd:;check

shr ax, 4; deslocamento a direita 
jmp next_cluter

is_oven:;check

and ax, 0x0fff

next_cluter:;check

cmp ax, 0x0ff8; compara máximo de cluter na fat
jae read_end ; terminou a leitura

mov [kernel_cluters], ax ; enquanto tiver cluter para ler continua o loop
jmp load_kernel_loop


read_end:;check

mov dl, [n_driver]; passando na memoria o fim do arquivo para ds
mov ax, kernel_segment
mov ds, ax
mov es, ax
jmp kernel_segment:kernel_offset


halt:

cli
hlt



;.read_disk:

;mov dl , [n_driver]
;mov ax , 1;lba
;mov cl , 1;ler o setor correponde do disco
;mov bx , buffer
;call disk_read
;jmp read_good



lba_to_chs:;check

push ax;salva o lba da pilha
push dx;salva o numero de drivers da pilha
xor dx, dx
div word [sector_p_track]; dividi lba pela quantidade de setores por trilha

inc dx;dx recebe lba%setorespor trilha +1
mov cx, dx

xor dx, dx
div word[head_face] ;dividi ax que conte conciente de lba pelo numero de cabeças

mov dh , dl; dh precisa ter a cabeça

mov ch , al ;os 8 bits da parte baixa vão se passado para ch
shl ah, 6; desloca os 6 primeiros bits para ah
or cl, ah ; os 2 bits restantes vão para cl faz parte numero cilindro alto

pop ax; passando a unidade dl para ax
mov dl, al; devolver o numero de unidade para dl

pop ax ;devolver o estado inicial do lba
ret




disk_read:

pusha ;pegar todos os registradores
push cx; garante que cx esteja no topo da pilha
call lba_to_chs


pop ax;passa a quantidade de setores a ler de cl para ax
mov ah, 0x02; função de leitura de disco
mov di, 3; armazena a quantidade de tentativas

.try:
;stc; ler cf para sinaliza erros
int 0x13; serviço de erros do care flags
jnc .done ; se a care flags subi vai para rotina .done

call disk_reset
dec di
test di, di
jnz .try

call fatal_error; 3 tentativas deu erro


.done:
popa; limpa a pilha e reseta os registradores
ret; retonar 

disk_reset:;check

pusha
mov ah , 0; função para renicia a unidade de disco
;stc
int 0x13
jnc .done
call fatal_error


.done:
popa; limpa a pilha e reseta os registradores
ret; retonar 



fatal_error:

;mov al, 0x03; modo de video
;int 0x10


mov si, msg_error
mov ah, 0x0e
mov cx, msg_len2

.read_char:
lodsb
int 0x10
loop .read_char

mov ah, 0; função de ler o teclado
int 0x16; espera a tecla se pressionada
jmp 0xffff:0000

read_good:

mov si, msg_ok
mov ah, 0x0e
mov cx, msg_len3

.read_char:
lodsb
int 0x10
loop .read_char




name_kernel: db 'KERNEL  BIN'
kernel_cluters dw 0
kernel_segment equ 0x2000; segremento para assume a escrita
kernel_offset equ 0
kernel_not: db ' kernel not found'
msg5_len: equ $ - kernel_not
msg_ok: db ' OK'
msg_len3: equ $ - msg_ok
msg_error: db ' FATAL ERROR: press any key to reboot'
msg_len2: equ $ - msg_error
msg: db 'loanding bootloader...'
msg_len equ $ - msg

pad:

times 510-($-$$) db 0

sig:
dw 0xaa55

buffer:
;apontando para último endereço da ram após mbr 