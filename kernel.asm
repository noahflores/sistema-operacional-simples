; -----------------------------------------------------------------------------
; LOS/T logo...
; -----------------------------------------------------------------------------
bits 16
org 0x0
; -----------------------------------------------------------------------------
main:
; -----------------------------------------------------------------------------
; Definir modo de vídeo (ax = 0x0013):
; - VGA 320x200, 256 cores
; - 8bits/pixel
; -----------------------------------------------------------------------------
	mov ax, 0x0013
	int 0x10
; -----------------------------------------------------------------------------
; Memória de vídeo
; Framebuffer linear (ES: 0xa000)
; -----------------------------------------------------------------------------
	push 0xa000  ; é o mesmo que: mov ax, 0xa000
	pop es       ;                mov es, ax
; -----------------------------------------------------------------------------
; limpar a tela (com preto: 0x00)
; -----------------------------------------------------------------------------
	xor ax, ax      ; Pixel de cor preta
	xor di, di      ; Posição inicial é o offset de ES
	mov cx, 320*200 ; Total de pixels na tela
	rep stosb       ; escreve CX vezes em [ES:DI] (framebuffer) o valor em AL
; -----------------------------------------------------------------------------
; Desenhar o logo...
; -----------------------------------------------------------------------------
    mov di, (320*72)+64 ; linha inicial
    xor ax, ax          ; Contagem de linhas dos sprites (0 a 6)
    mov si, bitmap      ; Endereço do bitmap em SI
lines_loop:
    mov cx, 4           ; Repetição das linhas dos sprites
.next_line:
    call draw_line      ; Imprime uma linha de pixels
    add di, 320         ; Vai para a linha seguinte
    dec cx              ; Reduz a contagem da repetição das linhas dos sprites
    test cx, cx         ; Verigica se a contagem zerou
    jnz .next_line      ; Se não zerou, imprime outra linha de pixels
    inc ax              ; Se zerou, incrementa a contagem de linhas de sprites
    cmp ax, 7           ; Verifica se passou de 7
    je halt             ; Se passou, termina o programa
    add di, 2*320       ; Se não passou, pula para 4 pixels abaixo
    add si, 24          ; Incrementa si em 24 bytes
    jmp lines_loop      ; Continua imprimindo linhas

    add di, 8*320       ; Pula para 8 pixels abaixo
; -----------------------------------------------------------------------------
halt:
; -----------------------------------------------------------------------------
	cli ; Limpa IF para que a CPU ignore interrupções externas.
	hlt ; Para a CPU
; -----------------------------------------------------------------------------
; SUBS
; -----------------------------------------------------------------------------
draw_line:
    pusha
    xor bx, bx              ; Índice do vetor da linha (0 a 23)
    mov cx, 7               ; Índice dos bits no byte (7 a 0)
.loop:
    xor ax, ax              ; Por padrão, AL tem valor 0
    bt [si + bx], cx        ; Testa o bit no índice BX e copia para CF
    jnc .write_pixel        ; Se o bit for 0, imprime pixel preto
    mov al, 2               ; Caso contrário, imprime pixel verde
.write_pixel:
    stosb                   ; Escreve o pixel no framebuffer
    dec cx                  ; Decrementa o índice dos bits
    cmp cx, 0               ; Compara com zero
    jge .loop               ; Se o índice forma maior ou igual a zero, continua imprimindo
    mov cx, 7               ; Caso contrário, redefine o índice do bit para 7
    inc bx                  ; Incrementa o índice do vetor da linha
    cmp bx, 24              ; Verifica se todos os 24 bytes foram impressos
    jne .loop               ; Se não foram, segue para o próximo byte
.done:
    popa
    ret
    
; -----------------------------------------------------------------------------
; DATA
; -----------------------------------------------------------------------------
bitmap:
    db 0xff,0xff,0xf0,0x00,0x00
    db 0x03,0xff,0xff,0xc0,0x00
    db 0x03,0xff,0xff,0xc0,0x00
    db 0x00,0x00,0x00,0xff,0x00
    db 0xff,0xff,0xff,0xff

    db 0x03,0xfc,0x00,0x00,0x00
    db 0xff,0x00,0x00,0xff,0x00
    db 0xff,0x00,0x00,0xff,0x00
    db 0x00,0x00,0x0f,0xf0,0x00
    db 0xf0,0x0f,0xf0,0x0f

    db 0x03,0xfc,0x00,0x00,0x00
    db 0xff,0x00,0x00,0xff,0x00
    db 0xff,0x00,0x00,0x00,0x00
    db 0x00,0x00,0xff,0x00,0x00
    db 0xc0,0x0f,0xf0,0x03

    db 0x03,0xfc,0x00,0x00,0x00
    db 0xff,0x00,0x00,0xff,0x00
    db 0x03,0xff,0xff,0xc0,0x00
    db 0x00,0x0f,0xf0,0x00,0x00
    db 0x00,0x0f,0xf0,0x00

    db 0x03,0xfc,0x00,0x03,0x00
    db 0xff,0x00,0x00,0xff,0x00
    db 0x00,0x00,0x00,0xff,0x00
    db 0x00,0xff,0x00,0x00,0x00
    db 0x00,0x0f,0xf0,0x00

    db 0x03,0xfc,0x00,0x0f,0x00
    db 0xff,0x00,0x00,0xff,0x00
    db 0xff,0x00,0x00,0xff,0x00
    db 0x0f,0xf0,0x00,0x00,0x00
    db 0x00,0x0f,0xf0,0x00
    
    db 0xff,0xff,0xff,0xff,0x00
    db 0x03,0xff,0xff,0xc0,0x00
    db 0x03,0xff,0xff,0xc0,0x00
    db 0xff,0x00,0x00,0x00,0x00
    db 0x00,0xff,0xff,0x00

